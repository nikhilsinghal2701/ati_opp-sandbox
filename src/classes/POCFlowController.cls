public with sharing class POCFlowController {

    public Flow.Interview.New_Job_Inquiry_2 theFlow { get; set; }

    public PageReference getFinishLocation() {
        PageReference pr = new PageReference('/');
        if (theFlow != null) {
            System.debug(theFlow.var_JobId);
            pr =  new PageReference('/' + theFlow.var_JobId + '/e');
        }
        return pr;        
    }
    
    @isTest public static void testPOCFlowController() {
        POCFlowController c = new POCFlowController();
        System.assertEquals(new PageReference('/').getUrl(), c.getFinishLocation().getUrl());
    }
}