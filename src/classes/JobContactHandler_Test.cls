/*
Version : 1.0
Company : CloudSherpas
Date : 19 AUG 2013
Author : Ma. Katrina B. Atanacio 
Description : Test class for JobContactHandler class
History : 1.0 - 19 AUG 2013 - MKBA - Created.
*/

@isTest
private class JobContactHandler_Test {
    static testMethod void testPopulateJobAccount(){
        //create custom setting TriggerStatus__c;
        
        TriggerStatus__c newTriggerStatus = new TriggerStatus__c ();
        newTriggerStatus.JobSetJobNumber__c = true;
        insert newTriggerStatus;
        
        //Create test Account 
        Account testAcctObj = new Account(); 
        testAcctObj.Name = 'Test Account'; 
        insert testAcctObj; 
        
        //Create test Contact 
        Contact testContactObj = new Contact(); 
        testContactObj.AccountId = testAcctObj.Id; 
        testContactObj.LastName = 'Test'; 
        insert testContactObj; 
        
        //Create test Job
        Job__c testJobObj = new Job__c(); 
        testJobObj.Job_Number__c = '000-00-00033';
        testJobObj.Stage__c = 'Estimate Only';
        testJobObj.Status__c = 'Opportunity';
        testJobObj.Name = '000-00-00033';  
        testJobObj.Job_Name__c = '000-00-00033';  
        
        insert testJobObj; 
        
        test.startTest(); 
        
        //Create test Job Contact 
        Job_Contact__c testJobContactObj = new Job_Contact__c(); 
        testJobContactObj.Job__c = testJobObj.Id;
        testJobContactObj.Contact__c = testContactObj.Id; 
        insert testJobContactObj; 
        
        //Assert Test Result
        Job_Contact__c insertedJC = [SELECT Job_Acct__c FROM Job_Contact__c WHERE Id = :testJobContactObj.Id]; 
        System.assertEquals(insertedJC.Job_Acct__c, testAcctObj.Id); 
        
        test.stopTest(); 
    }
}