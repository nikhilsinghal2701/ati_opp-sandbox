@istest
public class AccountTrigger_Test{

    public static TestMethod void myUnitTest(){
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = 'line1\\\\line2';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
    }
    public static TestMethod void myUnitTest2(){
     TriggerStatus__c newTriggerStatus = new TriggerStatus__c();
    
     newTriggerStatus.AccountSyncToTimberline__c = true;
     insert newTriggerStatus;
     
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = '\\\\';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
        
        newAcct = [SELECT Id,BillingCity,Send_To_Timberline__c FROM Account WHERE Id = :newAcct.Id];
        newAcct.Send_To_Timberline__c = true;
        newAcct.BillingCity = 'test city2';
        update newAcct;
    }
     public static TestMethod void myUnitTest3(){
     TriggerStatus__c newTriggerStatus = new TriggerStatus__c();
     newTriggerStatus.AccountGenerateCustomerNumber__c = true;
     newTriggerStatus.AccountSyncToTimberline__c = true;
     insert newTriggerStatus;
        Profile p= [Select id from Profile where Name = 'System Administrator' LIMIT 1];
        User u = new User(
            Alias = 'standt', 
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
       // system.runas(u){
            Account newAcct = new Account();
            newAcct.Name = 'testAcct';
            newAcct.BillingStreet = '';
            newAcct.BillingCity = 'Test city';
            insert newAcct;
       // }
        
    }
      public static TestMethod void myUnitTest4(){
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = 'test1\\\\';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
    }
     public static TestMethod void myUnitTest5(){
     TriggerStatus__c newTriggerStatus = new TriggerStatus__c();
    
     newTriggerStatus.AccountSyncToTimberline__c = true;
     insert newTriggerStatus;
     
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = '\\\\';
        newAcct.BillingCity = 'Test city';
        newAcct.Send_To_Timberline__c = true;
        insert newAcct;
        
        newAcct = [SELECT Id,BillingCity,Send_To_Timberline__c FROM Account WHERE Id = :newAcct.Id];
        update newAcct;
    }
}