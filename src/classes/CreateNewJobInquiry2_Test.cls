/**
 * @Auhtor : Narasimha (Perficient)
 * @Created: 09.APR.2015
 * @Desc   : Test Class for the CreateNewJobInquiry2 apex class
 */
@isTest
private class CreateNewJobInquiry2_Test {

    static testMethod void myUnitTest() {
       
      	//create Custom Settings Trigger Status
      	TestDataUtil.createCustomSettings_TriggerStatus();
        //create Account 
        list<Account> lstAccounts = TestDataUtil.createAccounts(2, false, new map<String,Object>());
        lstAccounts[0].type ='Property Owner'; 
    	insert lstAccounts;
    	
    	list<contact> lstContact = TestDataUtil.createContacts(2, false, new map<String,Object>(), lstAccounts[0].Id);
    	insert lstContact;
    	
    	list<Opportunity> lstOppty = TestDataUtil.createOpportunity(4, false, new map<String,Object>(), lstAccounts[0].Id);
    	
    		lstOppty[0].contact__c = lstContact[0].id;
   		    lstOppty[0].Project_Site_Contact_Name__c = lstContact[1].id;
    		lstOppty[0].Insurance_Adjuster_Broker__c = lstContact[0].id;
			lstOppty[0].Primary_Account__c = 'Caller Account';
			
			lstOppty[1].contact__c = lstContact[0].id;
    		lstOppty[1].Insurance_Adjuster_Broker__c = lstContact[0].id;
			lstOppty[1].Primary_Account__c = 'Insurance';
			
			lstOppty[2].contact__c = lstContact[0].id;
    		lstOppty[2].Insurance_Adjuster_Broker__c =null;
			lstOppty[2].Primary_Account__c = 'Insurance';
			
			lstOppty[3].contact__c = lstContact[0].id;
			lstOppty[3].Primary_Account__c = 'Insurance';
			lstOppty[3].Insurance_Adjuster_Broker__c =null;
			
		PageReference pageRef = page.NewJob2;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', lstOppty[0].id);
		
		Test.startTest();
		
    		CreateNewJobInquiry2 testObj = new CreateNewJobInquiry2();
	   		    testObj.job = lstOppty[0];
			    testObj.Sameascaller = false;
			    testObj.save(); 
			    
			 ApexPages.currentPage().getParameters().put('id', lstOppty[1].id);
		 
		    CreateNewJobInquiry2 testObj2 = new CreateNewJobInquiry2();
			    testObj2.job = lstOppty[1];
			    testObj2.Sameascaller = true;
			    testObj2.save();       
			    
			  ApexPages.currentPage().getParameters().put('id', lstOppty[2].id);   
			    CreateNewJobInquiry2 testObj3 = new CreateNewJobInquiry2();
			    testObj3.job = lstOppty[1];
			    testObj3.Sameascaller = true;
			    testObj3.save();      
			 
			  insert lstOppty[3];
			  lstOppty[3].Add_Correct_Email__c ='test@test.com';
			  lstOppty[3].Contact__c =lstContact[1].Id;
			  lstOppty[3].Project_Site_Contact_Account__c =lstAccounts[0].Id;
			  lstOppty[3].AccountId =lstAccounts[1].Id;
			 try{
			  	update lstOppty[3];
			 }catch(Exception ex){
			 	
			 }
		Test.stopTest();
    }
    
}