@isTest (seealldata = true)
                    
public class CreateNewJobInquiry_Test {

    public static testMethod void TestCreateNewJobInquiry () {
        test.starttest();
        PageReference pageRef = page.NewRecord_With_Dupecatcher;
        Test.setCurrentPage(pageRef);
      try{
        Contact contact = new Contact();
        Account account = new account(billingStreet = contact.mailingstreet, billingcity = contact.mailingCity, billingstate = contact.mailingcity, billingpostalcode = contact.mailingpostalcode, phone = contact.phone);
        account.name = 'CIG';
        insert account;
        
        Contact.contact_type__c = 'Adjustor';
        contact.firstname = 'semira';
        contact.Lastname = 'roy';
        contact.accountid = account.id;
        insert contact;
    
        Contact contact2 = new Contact();
        Contact2.contact_type__c = 'Adjustor';
        contact2.Lastname = 'noname';
        contact2.firstname = 'noname';
        contact2.accountid = account.id;  
        insert contact2;
        
    
    Job__c job = new Job__c(account__c = account.id, Name = 'semira', Job_Name__c = 'test', Project_Site_Address__c = contact.mailingstreet, Project_Site_City__c = 'Los Angeles', Project_Site_state__c = 'CA', Project_Site_zipcode__c ='90027', County__c = 'LA', City_of_LA__c = 'Yes');
    job.contact__c = contact.id;
    job.Project_Site_Contact_Name__c = contact2.id;
    job.Insurance_Adjuster_Broker__c = contact.id;
	job.Primary_Account__c = 'Caller Account';

    ApexPages.currentPage().getParameters().put('qp', job.id);
    CreateNewJobInquiry testObj = new CreateNewJobInquiry();
    
    testObj.job = job;
    testObj.Sameascaller = false;
    testObj.save();   
    
    Job__c job2 = new Job__c(account__c = account.id, Name = 'sa', Job_Name__c = 'test', Project_Site_Address__c = contact.mailingstreet, Project_Site_City__c = 'Los Angeles', Project_Site_state__c = 'CA', Project_Site_zipcode__c ='90027', County__c = 'LA', City_of_LA__c = 'Yes');
    job2.contact__c = contact.id;
    //job.Project_Site_Contact_Name__c = contact2.id;
    job2.Insurance_Adjuster_Broker__c = contact.id;
	job2.Primary_Account__c = 'Insurance';

    ApexPages.currentPage().getParameters().put('qp', job2.id);
 
    CreateNewJobInquiry testObj2 = new CreateNewJobInquiry();
	
    testObj2.job = job2;
    testObj2.Sameascaller = true;
    testObj2.save();  
    test.stoptest();
}
      catch(Exception e) {

       }   
    } 
}