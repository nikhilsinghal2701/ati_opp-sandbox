/******************************************************************************
 * @summary: Controls connection between Salesforce and Timberline.
 *
 * TimberlineControl is used to control all communication between Salesforce
 * and Timberline using outbound calls to Timberline. It is assumed that SFDC
 * controls all communications between the two systems.
 *
 */
public with sharing virtual class TimberlineControl {
    
    
    
    
    /**************************************************************************
     * @summary: Makes a callout to Timberline.
     *
     * Performs a single callout to Timberline, sending over objects or
     * requesting data.
     *
     */
    protected void makeCallout() {
    	return;
    }
    
    
    
    /**************************************************************************
     * @summary: Sends Salesforce records to Timberline.
     *
     * Gathers Salesforce records and puts them into a form that can be sent
     * to Timberline using the {@link #makeCallout} function.
     *
     */
    protected void pushRecords() {
    	return;
    } 
    
    
    
    /**************************************************************************
     * @summary: Performs functionality common to all Timberline record pushes.
     *
     * 
     *
     */
    protected void pullRecords() {
    	return;
    }
    
    
    
    /**************************************************************************
     * @summary: Sends a list of Job records to Timberline
     *
     * Using the {@link #sendRecords} function, this sends Salesforce Job
     * records to Timberline.  After a Job is successfully sent, it will have
     * its TBD field updated to reflect that it was sent.
     *
     * @param jobs:
     *     a list of Job records that will be sent to Timberline. They
     *     will be modified by the function in the event that the transfer is
     *     successful to indicate the last successful data push.
     */
    public void sendJobs(List<Job__c> jobs) {
    	return;
    }
    
    
    
    /**************************************************************************
     * @summary: Sends a list of Forecast records to Timberline
     *
     * Using the {@link #sendRecords} function, this sends Salesforce Forecast
     * records to Timberline.  After a Forecast is successfully sent, it will 
     * have its TBD field updated to reflect that it was sent.
     *
     * @param forecasts:
     *     a list of Forecast records that will be sent to Timberline. They
     *     will be modified by the function in the event that the transfer is
     *     successful to indicate the last successful data push.
     *
     */
    public void sendForecasts(List<Forecast__c> forecasts) {
        return;	
    }
    
    
}