/* @Author: Narasimha (Perficient)
 * @Created: 17.MAR.2015
 * @Desc: Handler class for the Opportunity Trigger
 *
 */

public class OpportunityTriggerHandler {
	
	public static TriggerStatus__c cfg = TriggerConfig.raw;
	public static String opprtyOwnerRole ='Opportunity Owner';
	
	public static void handleBeforeInsertOnly(){
   		 // PREVENT DUPLICATE JOB NUMBERS ON CLONE
		 if (cfg.JobSetJobNumber__c && Trigger.isBefore && Trigger.isInsert) {
       		 System.debug(Logginglevel.INFO, 'JobTrigger: Running OpportunityJobControl.preventDuplicateJobNumbers...');
        		OpportunityJobControl.preventDuplicateJobNumbers(Trigger.new);
   			 }
	}

	public static void handleAfterInsertOnly(){
		
	}
	
	/* handles before updates only */
	public static void handleBeforeUpdatesOnly(){
		 map<Id,Opportunity> oldOpptyMap =(map<Id,Opportunity>)Trigger.oldMap;
		 map<Id,Opportunity> newOpptyMap =(map<Id,Opportunity>)Trigger.newMap;
		OpportunityTriggerHandler.EmailUpdateOnJob(oldOpptyMap,newOpptyMap);
	}
	
	
	
	/* handles both before insert and updates */
	public static void handleBeforeInsertUpdate(){
	    // Update Job Number
	    if (cfg.JobSetJobNumber__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
	     
	      map<Id,Opportunity> oldMap =null;
	      if(Trigger.isUpdate){
	      	oldMap =(map<Id,Opportunity>)Trigger.oldMap;
	      }
	      OpportunityJobControl.setJobNumberSequential(Trigger.new, (Trigger.isInsert) ? null : oldMap);
	      
	        
	        System.debug(Logginglevel.INFO, 'opportunityTrigger: Running OpportunityJobControl.recalculateJobNumbers...');
	        OpportunityJobControl.recalculateJobNumbers(Trigger.new, (Trigger.isInsert) ? null : oldMap);
	        
	        System.debug(Logginglevel.INFO, 'opportunityTrigger: Running OpportunityJobControl.setGLAccountPrefix...');
	        OpportunityJobControl.setGLAccountPrefix(Trigger.new);
	        
	    }
	    
	    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
	   		//logic - copies the Jobs Project Manager into the Owner field
	    	 OpportunityTriggerHandler.ManagerToOwnerCopy(Trigger.new);
	    }
	}
	
	/* handles both After insert and updates */
	public static void handleAfterInsertUpdate(){
			removeOpptyOwnerfromTeam(Trigger.new);
	}
	
	
	
	
	/*************************************************************************/
	/*Helper Methods ****************/	
	
	public static void ManagerToOwnerCopy(list<Opportunity> lstNewOpportunities){
	//logic to copies the Jobs Project Manager into the Owner field	    
	    if (! TriggerConfig.raw.JobCopyPMToOwner__c) {
    		return;
		}
    
	    //get all of the jobs associated with the trigger
	    Opportunity[] jobs = lstNewOpportunities;
	    
	    //loop through all of the jobs
	    for (Opportunity j : jobs)
	    {
	        //if project manager isn't empty
	        if(j.Project_Manager__c != null)
	        {
	            //copy the manager ID to the owner
	            j.OwnerId = j.Project_Manager__c;
	        }
	    }	
	}
	
	public static void EmailUpdateOnJob(map<Id,Opportunity> mapOldOppty, map<Id,Opportunity> mapNewOppty){
		
		
		//Limit the size of list by using Sets which do not contain duplicate elements
		  set<Id> ContactIds = new set<Id>();
		  set<ID>ProjectContactID = new set<Id>();
		  
		  Set<ID> updateContactIds = new Set<ID>();
		
		  String Email = '';
		  ID jobId = null;
		
		  
		  //When adding new Jobss or updating existing Jobs
		  if(trigger.isUpdate){
		    for(Opportunity p : mapNewOppty.values()){
		      email = p.Add_Correct_Email__c;
		      jobId = p.id;
		      
		      if(mapOldOppty.get(p.id).Add_Correct_Email__c != email){
		        ContactIds.add(p.Project_site_contact_name__c);
		      }     
		            
		      if(mapOldOppty.get(p.id).Project_site_contact_name__c != p.Project_site_contact_name__c){        
		        updateContactIds.add(p.Id);
		      }
		      else if(mapOldOppty.get(p.id).Project_Site_Contact_Account__c != p.Project_Site_Contact_Account__c){
		        updateContactIds.add(p.Id);
		      }
		      else if(mapOldOppty.get(p.id).AccountId != p.AccountId){
		        updateContactIds.add(p.Id);
		      }
		      else if(mapOldOppty.get(p.id).Contact__c != p.Contact__c){
		        updateContactIds.add(p.Id);
		      }
		    }
		
		if (updateContactIds.size() > 0)
		{
		  List<Opportunity> jobs = [SELECT ID, Contact__c, AccountId, Project_site_contact_name__c, Project_Site_Contact_Account__c, (SELECT ID, Contact__c FROM Job_Contacts__r), (SELECT ID, Account__c FROM Job_Accounts__r) FROM Opportunity WHERE ID in :updateContactIds];
		  List<Job_Contact__c> addJobContacts = new List<Job_Contact__c>();
		  List<Job_Account__c> addJobAccounts = new List<Job_Account__c>();
		  for (Opportunity job : jobs)
		  {    
		    // Check and change
		    // check Contact existed
		    boolean SameAsPrimaryContact = false;
		    boolean SameAsPrimaryAccount = false;
		    boolean contactExist = false;
		    boolean projectContactExist = false;
		    boolean accountExist = false;
		    boolean projectAccountExist = false;
		    
		    if(job.Contact__c == job.Project_site_contact_name__c)
		      SameAsPrimaryContact = true;
		      system.debug('This is SameAsPrimaryContact:::::::::::::::'+ SameAsPrimaryContact);
		    if(job.AccountId == job.Project_Site_Contact_Account__c)
		      SameAsPrimaryAccount = true;
		      system.debug('This is SameAsPrimaryAccount:::::::::::::::'+ SameAsPrimaryAccount);
		      
		    for(job_contact__c con : [select id, contact__c from job_contact__c where id in: job.job_contacts__r])
		    {
		      if (con.contact__c == job.Contact__c && con.contact__c != null)
		      {
		        contactExist = true;
		        system.debug('This is ContactExist:::::::::::::::'+ contactExist);
		      }if (con.contact__c == job.Project_site_contact_name__c && con.contact__c != null && job.Contact__c != job.Project_site_contact_name__c)
		      {
		        projectContactExist = true;
		        system.debug('This is projectContactExist:::::::::::::::'+ projectContactExist);
		      }
		      if (contactExist && projectContactExist)
		      {
		        break;
		      }
		    }
		    for(job_account__c acct : [select id, account__c from job_account__c where id in: job.job_accounts__r])
		    {
		      if (acct.account__c == job.AccountId && acct.account__c != null)
		      {
		        accountExist = true;
		        system.debug('This is accountExist:::::::::::::::'+ accountExist);
		      }if (acct.account__c == job.Project_Site_Contact_Account__c && acct.account__c != null && job.AccountId != job.Project_Site_Contact_Account__c)
		      {
		        projectAccountExist = true;
		        system.debug('This is projectAccountExist:::::::::::::::'+ projectAccountExist);
		      }
		      if (accountExist && projectAccountExist)
		      {
		        break;
		      }
		    }
		    
		    if (!contactExist){
		      Job_contact__c jobContact = new Job_Contact__c();
		      jobContact.Opportunity__c = job.Id;
		      jobContact.Contact__c = job.Contact__c;
		      addJobContacts.add(jobContact);
		    }
		    
		    if (!projectContactExist && !SameAsPrimaryContact)
		    {
		      Job_contact__c jobContact = new Job_Contact__c();
		      jobContact.Opportunity__c = job.Id;
		      jobContact.Contact__c = job.Project_site_contact_name__c;
		      addJobContacts.add(jobContact);      
		    }
		    if (!accountExist){
		      Job_Account__c jobAccount = new Job_Account__c();
		      jobAccount.Opportunity__c = job.Id;
		      jobAccount.account__c = job.AccountId;
		      addJobAccounts.add(jobAccount);
		    }
		    
		    if (!projectAccountExist && !SameAsPrimaryAccount)
		    {
		      Job_Account__c jobAccount = new Job_Account__c();
		      jobAccount.Opportunity__c = job.Id;
		      jobAccount.Account__c = job.Project_Site_Contact_Account__c;
		      addJobAccounts.add(jobAccount);      
		    }
		    
		    // Check Project Contact is exsted.
		  }
		  if (addJobContacts.size() > 0 )
		  {
		    insert addJobContacts;
		  }
		  if(addJobAccounts.size() > 0)
		  {
		    insert addJobAccounts;
		  }
		}
		
		 List<Contact> contactNeedActions = new list<contact>();
		
		    if (ContactIds.size() > 0){
		      
		      for(Contact a : [Select Id, email from Contact where Id IN :ContactIds]){
		
		        System.debug('Email is::::::::::::::::::::::::::::::::::::::' + a.email);
		        System.debug('Contact Id is::::::::::::::::::::::::::::::::::::::' + a.Id);
		        
		        a.email = email;
		        
		         contactNeedActions.add(a);
		
		      }
		      
		      update contactNeedActions;
		    }
		   
		  }
	}
	
	
	/*
	* @Method: removeOpptyOwnerfromTeam
	* @ Functionality: remove opporutnity owner from the opportunity team if the owner exists in the team as "Opportunity Owner" and Opportunity Project Manager is not blank 
	*/
	
	public static void removeOpptyOwnerfromTeam(list<Opportunity> lstNewOppty){
		
		set<Id> opptyIds = new set<Id>();
		map<Id,Opportunity> mapOppty = new map<Id,Opportunity>();
		list<OpportunityTeamMember> lstDeleteOpptyTeam = new list<OpportunityTeamMember>();
		for(Opportunity oppty:lstNewOppty){
			if(oppty.Project_Manager__c !=null){
				opptyIds.add(oppty.Id);
				mapOppty.put(oppty.Id,oppty);
			}
		}
		
		//Query for Owner
		list<OpportunityTeamMember> lstOpptyTeam = [select UserId, TeamMemberRole, OpportunityId, Id from  OpportunityTeamMember 
																									  where OpportunityId IN :opptyIds 
																									  AND  TeamMemberRole = :opprtyOwnerRole];
																									  
		for(OpportunityTeamMember opptyMem :lstOpptyTeam ){
			Opportunity oppty = mapOppty.get(opptyMem.OpportunityId);
			if(oppty!=null && (oppty.OwnerId != opptyMem.UserId )){
				//do not delete the current Owner
				lstDeleteOpptyTeam.add(opptyMem);
			}
		}
		if(lstDeleteOpptyTeam.size()>0){
			try{
				delete lstDeleteOpptyTeam;
			}catch(DmlException dmlex){
				for(Integer iFailedRow=0; iFailedRow<dmlex.getNumDml();iFailedRow++){
					Integer Orginalindex = dmlex.getDmlIndex(iFailedRow);
					OpportunityTeamMember opptyTeamMem = lstDeleteOpptyTeam[Orginalindex];
					Opportunity oppty = mapOppty.get(opptyTeamMem.OpportunityId);
					oppty.addError('Failed to remove the Opportunit Owner from team -'+ dmlex.getDmlMessage(iFailedRow) +' - Stacktrace::'+dmlex.getStackTraceString());
				}
			}
		}
		
	}

}