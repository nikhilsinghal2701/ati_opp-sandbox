global without sharing class BatchRetrofitJobNumberGroups implements Database.batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext info)
    { 
        String q = 'SELECT Id, Date_Time_Taken__c, CreatedDate, Office__c, Job_Number_Sequence_Group__c '
                 + 'FROM Job__c '
                 + 'WHERE Job_Number_Sequence_Group__c = null';
        return Database.getQueryLocator(q);
    } 
    global void execute(Database.BatchableContext info, List<Job__c> scope)
    {
       JobControl.setJobNumberIncGroups(scope);
       update scope;
    }
    global void finish(Database.BatchableContext info)
    {
    	return;
    }
    public static void quickStart()
    {
    	BatchRetrofitJobNumberGroups b = new BatchRetrofitJobNumberGroups();
        Id batchInstanceId = Database.executeBatch(b, 200);
        System.debug(LoggingLevel.INFO, 'BatchRetrofitJobNumberGroups.quickStart: Started Batch ID: '+batchInstanceId);
    }
}