/********************************************************************************
*
*   SynchForecastBatch: class that allows Salesforce to synch all the Forecast in 
*                       the system.
*
*   MUST READ:  YOU MUST CALL THIS METHOD WITH A LIMIT OF 20 OR I AM NOT SURE WHAT
*               WILL HAPPEN (Note: Doesn't seem like a big deal, can get up to
*               500 or so without issue)
*
*********************************************************************************/
global class SynchForecastBatch implements Database.batchable<sObject>, Database.AllowsCallouts, Schedulable, Database.Stateful
{
    
    // Note that LAST_N_YEARS is based on the calendar year, not 365 days.  It
    // starts at the previous year and begins counting from there. For example,
    // On February 2013, this query will get any jobs after 1/1/2010
    global static final String QUERY = 'Select id, Update_Forecast__c from Job__c '
                                     + 'WHERE (Date_Time_Taken__c != NULL AND Date_Time_Taken__c >= LAST_N_YEARS:3) '
                                     + 'OR (Date_Time_Taken__c = NULL AND CreatedDate >= LAST_N_YEARS:3)';
    
    global static final String QUERY_ALL = 'Select id, Update_Forecast__c from Job__c';
    
    
    
    /**************************************************************************** 
    *   List of email addresses who should receive notice when Forecast
    *  refresh is initiated and when it completes.  Note that all recipients'
    *  email addresses will be included in the "TO" line of the email 
    *******************************************************************************/
    private static List<String> REFRESH_NOTIFICATION_RECIPIENTS = new List<String> {
        'jeff.dorner@amer-tech.com'
    };
    
    
    // Default the query to this
    public String q = SynchForecastBatch.QUERY;
    
    // Fast mode is a different selection that only pulls jobs who have either
    // been updated or have invoices that have been updated since the last time
    // the job had its forecasts updated.  This should pull a much smaller
    // record set (on the order of a few hundred) so the refresh should happen
    // in seconds, not minutes.
    public Boolean useFastMode = false;
    public DateTime startedDate = null;
    
    
    /************************************************************************
    *   
    *   SynchForecastBatch: default Constructor
    *
    *************************************************************************/ 
    global SynchForecastBatch(String pq)
    {
        this.q = pq;
        
        If(TestClassHelper.isTestClassExecuting)
            this.q='Select id, Update_Forecast__c from Job__c '
                                     + 'WHERE (Date_Time_Taken__c != NULL AND Date_Time_Taken__c >= LAST_N_YEARS:3) '
                                     + 'OR (Date_Time_Taken__c = NULL AND CreatedDate >= LAST_N_YEARS:3) LIMIT 5';
        
        
        this.startedDate = DateTime.now();
    }
    
    global SynchForecastBatch()
    {
        this.q = SynchForecastBatch.QUERY;
        
         If(TestClassHelper.isTestClassExecuting)
            this.q='Select id, Update_Forecast__c from Job__c '
                                     + 'WHERE (Date_Time_Taken__c != NULL AND Date_Time_Taken__c >= LAST_N_YEARS:3) '
                                     + 'OR (Date_Time_Taken__c = NULL AND CreatedDate >= LAST_N_YEARS:3) LIMIT 5';
        
        this.startedDate = DateTime.now();
    }
    
    
    /************************************************************************   
    *
    *   start:  method that will kick the batch proccess. It will run the
    *           query without limits, and pass that query locator to the 
    *           excute method
    *   
    *   PARAMS: 
    *           Database.BatchableContext info
    *
    *   RETURN:
    *
    *           Database.QueryLocator:  Query locator that will contain all
    *                                   the jobs in the system.
    *
    **************************************************************************/
    global Iterable<sObject> start(Database.BatchableContext info)
    {
        // Fast mode is a way to do an incremental refresh of jobs that may
        // miss some data of which Salesforce is not aware.  Essentially, it
        // will attempt to pull forecast updates ONLY for those jobs that have
        // either been updated since the last forecast refresh or have had
        // invoices updated since the last forecast refresh.  This special case
        // ignores the query property of the object.
        
        // Due to query limits, fast mode will really only work for a small
        // number of records (a few thousand).  This assumption was verified
        // by ATI.
        if (this.useFastMode == true) {
            
            List<Invoice__c> inv = [SELECT Id, Job__c FROM Invoice__c WHERE Is_Forecast_Quick_Update_Eligible__c = 'true'];
            
            Set<Id> jids = new Set<Id>();
            for (Invoice__c i : inv) {
                jids.add(i.Job__c);
            }
            
            return [SELECT id, Update_Forecast__c FROM Job__c WHERE Id in :jids];
            
        } else {
            return Database.getQueryLocator(this.q);
        }
    }     
    
    /*************************************************************************
    *
    *   execute:    method that will be called after start. It will run the 
    *               synch method on the forecast.
    *           
    *   PARAMS:
    *               Database.BatchableContext info:
    *               
    *               List<Job__c> scope: List of objects that need to be synch
    *
    *   RETURNS:    Nothing
    *
    *************************************************************************/
    global void execute(Database.BatchableContext info, List<Job__c> scope)
    {
       System.debug('SynchForecastBatch.execute starts ');
       
       Set <Id> jobsToUpdate = new Set<Id>();
       
       for(Job__c j : scope)
       { 
           jobsToUpdate.add(j.id);
       } 
       
       TimberlineSynch.synchForecastNotFuture(jobsToUpdate); 
       
       CT_Logging.SaveLogs();
       
       System.debug('End.execute starts ');
    }     
    
    /*************************************************************************
    *
    *   finish:     method that will be called after execute. 
    *           
    *   PARAMS:
    *               Database.BatchableContext info:
    *               
    *
    *   RETURNS:    Nothing
    *
    *************************************************************************/
    global void finish(Database.BatchableContext info)
    {     
        // Make sure that we have notification recipients.  If not, end
        if (REFRESH_NOTIFICATION_RECIPIENTS == null || REFRESH_NOTIFICATION_RECIPIENTS.isEmpty()) 
        {
            System.debug(Logginglevel.INFO, 'SynchForecastBatch: Skipping notification because there are no recipients defined');
            return;
        }
        
        try 
        {
        
            // Generate the email to send
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(REFRESH_NOTIFICATION_RECIPIENTS);
            mail.setReplyTo('noreply@atirestoration.my.salesforce.com');
            mail.setSenderDisplayName('Salesforce Notifier');
            mail.setSubject('Synchronization of Forecast');
            
            // Notification body is different 
            mail.setPlainTextBody(' The Synchronization of Forecast initiated by '+ UserInfo.getFirstName() + ' ' + UserInfo.getLastName()+' ('+UserInfo.getUserId()+')'
                                  + ' at ' + startedDate.format()
                                  + ' has completed.\n');
            
            // Send the email
            Messaging.sendEmail(new Messaging.Email[] {mail});
            
        } 
        catch (Exception e) 
        {
            // Something failed sending a notification.  Honestly, this is far
            // from the end of the world.  Log+Suppress!
            System.debug(Logginglevel.WARN, 'SynchForecastBatch: Notification send failed! '+ e.getMessage()+'\n'+ e.getStackTraceString());            
        }
    } 
    
    
    /*************************************************************************
     * Executor for the schedulable interface
     *
     */
    global void execute(SchedulableContext SC) {
        SynchForecastBatch b = new SynchForecastBatch(SynchForecastBatch.QUERY_ALL);
        Id batchInstanceId = Database.executeBatch(b, 500);
    }
}