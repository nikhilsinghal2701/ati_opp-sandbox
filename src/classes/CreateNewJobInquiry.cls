public with sharing class CreateNewJobInquiry {

    public String SelectPrimaryAccount { get; set; }
    public boolean SameAsCaller { get; set; }
    
    list<Job_Account__c> jAcct = new list<Job_account__c>();
    list<job_Contact__c> jCon = new list<Job_contact__c>();
    
    Public Job__c job { get; set;}

    
    public CreateNewJobInquiry() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        
        job = new Job__c(name = 'new');    
    }

        
    public PageReference cancel() {
        return null;
    }


    public PageReference Save() {
           
        try
        {
        //Assigning Primary contact and account
           
            Id acctid = Job.Account__c;
                        
            job_Contact__c c1; job_Account__c a1;
            
            Contact con = [select id, accountid, account.type from Contact where id =:Job.Contact__c];
                c1 = new job_Contact__c(contact__c = con.id);
                a1 = new Job_Account__c(account__c = con.accountid);
                
                if(job.Primary_Account__c == 'Caller Account'){
                            job.account__c = con.accountid;
                }

            
            job_Contact__c c2; job_Account__c a2; Contact con2 = null;
            
            if(SameAsCaller != true && job.Project_Site_Contact_Name__c != null){
                
                con2 = [select id, accountid, account.type from Contact where id =:job.Project_Site_Contact_Name__c limit 1];           
                System.debug('THIS IS THE SAME AS CALLER:::::::::::::::::::::::::::::' + sameascaller);
                            
                job.Project_Site_Contact_Account__c = con2.accountid;
                
                c2 = new job_Contact__c(contact__c = con2.id);
                a2 = new Job_Account__c(account__c = con2.accountid);
                
                System.debug('THIS IS THE 2ND CONTACT:::::::::::::::::::::::::::::' + c2.contact__c);
                System.debug('THIS IS THE Contact 2:::::::::::::::::::::::::::::' + con2.id);
                
                
                if(job.Primary_Account__c == 'Project Site Contact'){
                        job.contact__c = job.Project_Site_Contact_Name__c;
                        job.account__c = con2.accountid;
                }
            }
            else {
                job.Project_Site_Contact_Name__c = con.id;
                job.Project_Site_Contact_Account__c = con.accountid;
                job.account__c = con.accountid;
            }
            
            job_Contact__c c3; job_Account__c a3; Contact con3 = null;
            if(job.Insurance_Adjuster_Broker__c != null){
                con3 = [select id, accountid, account.type from Contact where id =:job.Insurance_Adjuster_Broker__c limit 1];
                c3 = new job_Contact__c(contact__c = con3.id);
                a3 = new Job_Account__c(account__c = con3.accountid);
                
                   
                if(job.Primary_Account__c == 'Insurance'){
                        job.contact__c = job.Insurance_Adjuster_Broker__c;
                        job.account__c = con3.accountid;
                }
            }
            else{
                if(acctid != null){
                    a3 = new Job_Account__c(account__c = acctid);
                    
                    if(job.Primary_Account__c == 'Insurance'){
                        job.account__c = acctid;
                        job.contact__c = null;}
                }
            }
            
        job.Taken_by__c = UserInfo.getUserId();
        job.created_by__c = UserInfo.getUserId();
 
 
        insert job;
      
        
        //assigning Job Role
         if(con.account.type == 'Property Owner')
             c1.Job_role__c = 'Bill To';
         if(c2 != null && con2.account.type == 'Property Owner')
               c2.job_role__C = 'Bill To';

       
       //inserting the Job Account and Job Contact related list
        
            c1.job__c = job.id;
            a1.job__c = job.id;
            jcon.add(c1);jacct.add(a1);
            
          if(con2 != null && (con2.id != con.id)){
            System.debug('THIS IS THE INSIDE THE LOOP:::::::::::::::::::::::::::::' + c2.id);
            
            c2.job__c = job.id;jcon.add(c2);
            if(con2.AccountId != con.AccountId){
                a2.job__c = job.id; jacct.add(a2);
          }
                
            }
        
            if(a3 != null){
                if(acctid != null && (acctid != con.AccountId || (con2 != null && acctid != con2.AccountId))){
                    system.debug('THIS IS ACCOUNT 3:::::::::::::::::::::::::::::' + a3.Account__r.id);
                    a3.job__c = job.id; jacct.add(a3);
                }
                else{
                    if(con3 != null && (con3.id != con.id || (con2 != null && con3.id != con2.id))){
                    c3.job__c = job.id;
                    a3.Job__c = job.id;
                    jcon.add(c3); jacct.add(a3);
                    
                    }
                }
            }   
            
          
            insert jcon;
            insert jacct;
            
            System.debug('THIS IS THE 2ND CONTACT:::::::::::::::::::::::::::::' + jcon);
            //System.debug('THIS IS THE 3rd CONTACT:::::::::::::::::::::::::::::' + c2.id);
        
            
        PageReference ReturnPage = new PageReference('/' + job.id + '/e?retURL=%2F'+job.id); ReturnPage.setRedirect(true);
        return ReturnPage;
        
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    }

}