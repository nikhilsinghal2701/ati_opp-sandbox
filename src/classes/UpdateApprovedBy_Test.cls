@isTest
public class UpdateApprovedBy_Test{

    public static TestMethod void myUnitTest(){
    
        Expense__c expense = new Expense__c(name = 'test', date__c = system.today(), Status__c = 'Submitted', Date_Submitted__c = system.today());
        insert expense; 
        system.debug('this is insert::::::::::::::::::::::::'+ expense);
        
    // Create an approval request for the account
    Approval.ProcessSubmitRequest req1 = 
        new Approval.ProcessSubmitRequest();
    req1.setComments('Submitting request for approval.');
    req1.setObjectId(expense.id);

    // Submit the approval request for the account
    Approval.ProcessResult result = Approval.process(req1);

    // Verify the result
    System.assert(result.isSuccess());

    System.assertEquals(
        'Pending', result.getInstanceStatus(), 
        'Instance Status'+result.getInstanceStatus());

    // Approve the submitted request
    // First, get the ID of the newly created item
    List<Id> newWorkItemIds = result.getNewWorkitemIds();

    // Instantiate the new ProcessWorkitemRequest object and populate it
    Approval.ProcessWorkitemRequest req2 = 
        new Approval.ProcessWorkitemRequest();
    req2.setComments('Approving request.');
    req2.setAction('Approve');
    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

    // Use the ID from the newly created item to specify the item to be worked
    req2.setWorkitemId(newWorkItemIds.get(0));

    // Submit the request for approval
    Approval.ProcessResult result2 =  Approval.process(req2);

    // Verify the results
    System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());

    System.assertEquals(
        'Approved', result2.getInstanceStatus(), 
        'Instance Status'+result2.getInstanceStatus());


        
    }
}