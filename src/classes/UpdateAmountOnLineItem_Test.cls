@isTest
public class UpdateAmountOnLineItem_Test{
    
     
     public static TestMethod void myUnitTest(){
     
     Expense__c expense = new Expense__c(name = 'test', date__c = system.today());
     insert expense;
     
     Expense_Line_Item__c LineItem = new Expense_Line_Item__c();
     
     LineItem.Expense__c = expense.id;
     LineItem.Name = 'testing for Delete trigger';
     LineItem.Amount__c = 500;
     LineItem.Contact__c ='00370000015j7G8';
     LineItem.Date_Of_Expense__c = system.today();
     
     insert LineItem; 
     
     Expense_Line_Item__c LineItem1 = [select id, contact__c, amount__c, Office__c from Expense_Line_Item__c where expense__c =:expense.id];
     LineItem1.amount__c = 45;
     LineItem1.Office__c = 'Chicago';
     
     Update LineItem1;
     
     Expense_Line_Item__c LineItem2 = [select id from Expense_Line_Item__c where expense__c =:expense.id];
     
     delete LineItem2;
    
    }
}