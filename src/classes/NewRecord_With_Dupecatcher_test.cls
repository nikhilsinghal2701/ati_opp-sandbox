@isTest(seealldata = true)
public class NewRecord_With_Dupecatcher_test{
    
    public static testMethod void testNewRecord_With_Dupecatcher(){

       try { 
        Account account = new Account(Name = 'test1234567890');
        insert account;
        
        Account act = [select id from Account where name = 'test1234567890' limit 1];
        
        Contact contact = new Contact(firstname = 'testing', lastname = 'test', Contact_type__c = 'Adjustor', accountId = act.id);
        Contact contact2 = new Contact(firstname = 'testing', lastname = 'test', Contact_type__c = 'Property Owner', accountId = act.id);

    Contact con = [select id, name from Contact limit 1];
   
        Test.startTest();

        Test.setCurrentPage(Page.NewRecord_With_Dupecatcher);

        NewRecord_With_Dupecatcher controller = new NewRecord_With_Dupecatcher(new ApexPages.StandardController(contact));
		NewRecord_With_Dupecatcher controller2 = new NewRecord_With_Dupecatcher(new ApexPages.StandardController(contact2));
        
       controller.save();
       controller2.save();

        Test.stopTest();     

        } catch(Exception e) {
            
        }
    }
}