@isTest

public class ContactTrigger_Test{
    public static TestMethod void UnitTest1(){
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = 'line1\\\\line2';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
        
        Contact newCont = new Contact();
        newCont.AccountId = newAcct.Id;
        newCont.LastName = 'test lname';
        newCont.MailingStreet = 'line1\\\\line2';
        insert newCont;
    }
      public static TestMethod void StreetEmpty(){
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = 'line1\\\\line2';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
        
        Contact newCont = new Contact();
        newCont.AccountId = newAcct.Id;
        newCont.LastName = 'test lname';
        newCont.MailingStreet = '\\\\';
        insert newCont;
    }
      public static TestMethod void StreetLine1(){
        Account newAcct = new Account();
        newAcct.Name = 'testAcct';
        newAcct.BillingStreet = 'line1\\\\line2';
        newAcct.BillingCity = 'Test city';
        insert newAcct;
        
        Contact newCont = new Contact();
        newCont.AccountId = newAcct.Id;
        newCont.LastName = 'test lname';
        newCont.MailingStreet = 'line1\\\\';
        insert newCont;
    }
}