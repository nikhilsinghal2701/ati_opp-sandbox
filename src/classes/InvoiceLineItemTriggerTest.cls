@istest(SeeAllData=true)
public class InvoiceLineItemTriggerTest{

    private static ID accountID;
    
        
    public static TestMethod void myUnitTest1()
    {
        
        User u = [select id,name from user where id =:System.label.TestClassRunAsUserId];
        
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type = 'Consultant';
        //acc.New_Job_Start_Date__c = system.now().date();
        //acc.Temporary_field__c = false;
        insert acc;
        accountID= acc.id;
        
        Contact con = new Contact();
        con.accountid = acc.id;
        con.contact_type__c = 'Consultant';
        con.FirstName = 'Test';
        con.lastname = 'Contact';
        insert con;
        
        //Create a job to associate with Invoice record
        Job__c job = new Job__c();
        job.Job_Name__c = 'Test Job 1';
        job.Name = 'Job 001';
        job.Stage__c = 'Qualification';
        job.Status__c = 'Opportunity';
        job.recordtypeid = '01270000000ULYa';
        job.account__c = acc.id;
        job.Date_Time_Taken__c = system.now().date();
        job.Sync_to_Timberline__c = true;
        job.Update_Forecast__c = true;
        job.Add_Correct_Email__c = 'test123@gmail.com';
        job.Project_Manager__c = UserInfo.getUserId();
   
   
   		Tax_Group__c tg = new Tax_Group__c();
   		tg.name = 'taxgrp001';
   		tg.Tax_Rate__c = 20;
   		tg.Timberline_Id__c = 'Timber001';
   		tg.Tax1Name__c = 'tax1';
   		tg.Tax1Rate__c=20;
   		tg.Tax2Name__c = 'tax2';
   		tg.Tax2Rate__c = 10;
   		tg.Tax3Name__c = 'tax3';
   		tg.Tax3Rate__c = 30;
   		tg.Tax4Name__c = 'tax4';
   		tg.Tax4Rate__c = 40;
   		tg.Tax5Name__c = 'tax5';
   		tg.Tax5Rate__c = 5;
   		tg.Tax6Name__c = 'tax6';
   		tg.Tax6Rate__c = 3;
   		tg.Tax7Name__c = 'tax7';
   		tg.Tax7Rate__c = 3;
   		tg.Tax8Name__c = 'tax8';
   		tg.Tax8Rate__c = 3;
   		tg.Tax9Name__c = 'tax9';
   		tg.Tax9Rate__c = 3;
   		tg.Refresh_Tax_Groups__c = true;
       
        System.runas(u)
        {
            Test.StartTest();
            insert job;
            insert tg;
            
            Job_Contact__c jobcon = new Job_Contact__c();
            jobcon.Job__c = job.id;
            jobcon.Contact__c = con.id;
            jobcon.Job_Acct__c = acc.id;
            jobcon.Job_Role__c = 'Insured';
            insert jobcon;
            
            
            
            
            //Create an invoice record to assiciate with invoice line item
            Invoice__c invoice = new Invoice__c();
            invoice.Name = 'Invoice 001';
            invoice.Job__c = job.id ;
            invoice.Due_Date__c = System.now().date();
            invoice.Bill_to_Tax_Group__c = tg.id;
            insert invoice;
            //invoice. = ;
            
            List<Invoice_Line_Item__c> listInvoiceLineItem1 = new List<Invoice_Line_Item__c>();
            Invoice_Line_Item__c ILI1 = new Invoice_Line_Item__c();
            ILI1.PickListName__c = 'Lead Abatement';
            ILI1.Description__c = 'Testing Class';
            ILI1.Amount__c = 100;
            ILI1.Invoice__c = invoice.id;
            ILI1.Order__c = 1;
            insert ILI1;
            
            ILI1.amount__c = 1000;
            ILI1.Order__c = 2;
            update ILI1;
            
            List<Invoice_Line_Item__c> listInvoiceLineItem2 = new List<Invoice_Line_Item__c>();
            Invoice_Line_Item__c ILI2 = new Invoice_Line_Item__c();
            ILI2.PickListName__c = 'Lead Abatement';
            ILI2.Description__c = 'Testing Class';
            ILI2.Amount__c = 100;
            ILI2.Invoice__c = invoice.id;
            ILI2.Order__c = 4;
            insert ILI2;
            
            
            ILI2.Order__c = 1;
            update ILI2;
            
            Test.StopTest();
       }
        
    }
    
    public static TestMethod void myUnitTest2()
    {
         User u = [select id,name from user where id =: System.label.TestClassRunAsUserId];
         system.runas(u){
         Test.StartTest();
         Opportunity Opp = new Opportunity();
         opp.accountid = accountID;
         opp.Name = 'Test Opportunity';
         opp.StageName = 'Qualification';
         opp.CloseDate = system.now().date().addDays(10);
         insert opp;  
         
         
         Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type = 'Consultant';
        //acc.New_Job_Start_Date__c = system.now().date();
        //acc.Temporary_field__c = false;
        insert acc;
        accountID= acc.id;
        
        Contact con = new Contact();
        con.accountid = acc.id;
        con.contact_type__c = 'Consultant';
        con.FirstName = 'Test';
        con.lastname = 'Contact';
        insert con;
        
        //Create a job to associate with Invoice record
        Job__c job = new Job__c();
        job.Job_Name__c = 'Test Job 1';
        job.Name = 'Job 001';
        job.Stage__c = 'Qualification';
        job.Status__c = 'Opportunity';
        job.recordtypeid = '01270000000ULYa';
        job.account__c = acc.id;
        job.Date_Time_Taken__c = system.now().date();
        job.Sync_to_Timberline__c = true;
        job.Update_Forecast__c = true;
        job.Project_Manager__c = UserInfo.getUserId();
         
        insert job;
            
            
            Job_Contact__c jobcon = new Job_Contact__c();
            jobcon.Job__c = job.id;
            jobcon.Contact__c = con.id;
            jobcon.Job_Acct__c = acc.id;
            jobcon.Job_Role__c = 'Insured';
            insert jobcon;
            
            
            
            
            //Create an invoice record to assiciate with invoice line item
            Invoice__c invoice = new Invoice__c();
            invoice.Name = 'Invoice 001';
            invoice.Job__c = job.id ;
            invoice.Due_Date__c = System.now().date();
            insert invoice;
            List<Invoice__c> lstInvoice = new List<Invoice__c>();
            lstInvoice.add(invoice);
            InvoiceLineItemControl.createLineItems(lstInvoice);
            
            Invoice_Line_Item__c ILI2 = new Invoice_Line_Item__c();
            ILI2.PickListName__c = 'Lead Abatement';
            ILI2.Description__c = 'Testing Class';
            ILI2.Amount__c = 100;
            ILI2.Invoice__c = invoice.id;
            ILI2.Order__c = 4;
            insert ILI2;
            
            
            ILI2.Order__c = 1;
            update ILI2;
               
         Test.StopTest();
         
         
         
         
         }
         
    }
    
    
}