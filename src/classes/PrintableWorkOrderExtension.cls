/** I'm really angry that this can't be done in pure VF... */
public with sharing class PrintableWorkOrderExtension {
	
    public static List<String> ADDL_FIELDS = new List<String> {'Id','Notes__c'};
    
    public String notes {get; set;}
    
    public PrintableWorkOrderExtension(ApexPages.StandardController controller) {
    	
        if (! Test.isRunningTest()) {
            controller.addFields(ADDL_FIELDS);
        }
        
        Job_Resource__c r = (Job_Resource__c) controller.getRecord();
        if (! String.isBlank(r.Notes__c)) {
            notes = r.Notes__c.escapeHtml4().replace('\n', '<br/>');
        }
    }
    
    
    
    
    private static testmethod void test_init() {
        
        try {
        	
        	String fields = ADCUtil_Base.strJoin(',', ADDL_FIELDS, 'Id');
	        Job_Resource__c r = (Job_Resource__c) Database.query('SELECT '+fields+' FROM Job_Resource__c ORDER BY LastModifiedDate DESC LIMIT 1');
	        
	        PageReference ref = Page.PrintableWorkOrder;
	        Test.setCurrentPage(ref);
	        Apexpages.StandardController std = new Apexpages.StandardController(r);
	        
	        Test.startTest();
	        
	        PrintableWorkOrderExtension controller = new PrintableWorkOrderExtension(std);
	        
	        Test.stopTest();
	        
        } catch (Exception e) {
        	
        	// It's, like, 4 lines... not worth losing sleep over if it fails
	    	System.debug(Logginglevel.WARN, 'PrintableWorkOrderExtension.test_init: TEST FAILED WITH EXCEPTION: '+e);
	    	
	    }
        
    }
    
}