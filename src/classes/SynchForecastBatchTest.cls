@isTest(SeeAllData=true)
private class SynchForecastBatchTest
{
    static testMethod void myUnitTest()
    {       
         TestClassHelper.isTestClassExecuting = true;
         User u = [select id,name from user where id =:System.label.TestClassRunAsUserId];
         System.runAs(u)
        {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type = 'Consultant';
        insert acc;
        
        
        Contact con = new Contact();
        con.accountid = acc.id;
        con.contact_type__c = 'Consultant';
        con.FirstName = 'Test';
        con.lastname = 'Contact';
        insert con;
        
        LIST<JOB__C> lstJobs = new List<Job__c>();
        for(integer i=1;i<5;i++)
        {
            //Create a job to associate with Invoice record
            Job__c job = new Job__c();
            job.Job_Name__c = 'Test Job 1';
            job.Name = 'Job 001';
            job.Stage__c = 'Qualification';
            job.Status__c = 'Opportunity';
            job.recordtypeid = '01270000000ULYa';
            job.account__c = acc.id;
            job.Date_Time_Taken__c = system.now().date();
            job.Sync_to_Timberline__c = true;
            job.Update_Forecast__c = true;
            job.Project_Manager__c = UserInfo.getUserId();
            lstJobs.add(job);
            
        }
       
            Test.StartTest();
            insert lstJobs;
         List<Invoice__c> lstInvoices = new list<Invoice__c>();
          for(integer i=1;i<5;i++)
          {
              
            
            Invoice__c invoice = new Invoice__c();
            invoice.Name = 'Invoice '+i;
            invoice.Job__c = lstJobs[i-1].id ;
            invoice.Due_Date__c = System.now().date();
            //invoice.Is_Forecast_Quick_Update_Eligible__c = 'true';
            lstInvoices.add(invoice);
          }      
            insert lstInvoices;
                
                SynchForecastBatch batchClass =new SynchForecastBatch();
                
                batchClass.start(null);
                batchClass.execute(null, lstJobs);
                batchClass.finish(null);
        }        
    }
}