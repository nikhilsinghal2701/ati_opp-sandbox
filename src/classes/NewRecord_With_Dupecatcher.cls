public class NewRecord_With_Dupecatcher {

    public Contact contact {get; set;}
    public Account account {get; set;}
    RecordType recType;
    RecordType recTypeBusiness;
    public ApexPages.StandardController controller;
    //String company;
     
    public NewRecord_With_Dupecatcher(ApexPages.StandardController controller) {
        
        this.contact = (contact)controller.getRecord();
        //company = contact.account.name;
    }

    public PageReference save() {
    
        PageReference redirect;
    
        recType = [select id,name,sobjectType,ispersontype from recordType where ispersontype=true and sobjectType='account' limit 1];
        recTypeBusiness = [select id,name,sobjectType,ispersontype from recordType where ispersontype=false and sobjectType='account' limit 1];
        
        Try{
            if(contact.contact_type__c == 'property owner')
            {
             account = new Account(recordtypeid = recType.ID, type = contact.contact_type__c, firstname = contact.firstname, lastname = contact.lastname, personemail = contact.email, phone = contact.phone, Phone_Ext__c = Contact.Phone_Ext__c, BillingStreet = contact.MailingStreet, BillingCity = contact.MailingCity, BillingState = contact.MailingState, BillingPostalCode = Contact.MailingPostalCode, BillingCountry = contact.MailingCountry);
             insert account;
             redirect= new PageReference('/'+ account.id);
            }
            else{
            
               /* account = new Account(recordtypeid = recType.ID, name = contact.account.name, phone = contact.phone, Phone_Ext__c = Contact.Phone_Ext__c, BillingStreet = contact.MailingStreet, BillingCity = contact.MailingCity, BillingState = contact.MailingState, BillingPostalCode = Contact.MailingPostalCode, BillingCountry = contact.MailingCountry);
                insert account;*/
                //contact.accountID = account.id;
                if(contact.contact_type__c == null)
                    contact.contact_type__c = 'Adjustor';
                insert contact;
                redirect= new PageReference('/'+ contact.id);
            }
        }    
        Catch(Exception e){
                ApexPages.addMessages(e);
                return null;
        }
        
    
        redirect.setRedirect(true);
        return redirect; 
    }

}