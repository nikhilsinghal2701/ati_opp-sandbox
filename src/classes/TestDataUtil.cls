public class TestDataUtil {
	
	//create Users
	public static List<User> createUsers(Integer numToInsert,
		Boolean doInsert,
		Map<String, Object> nameValue) {
			List<User> users = new List<User>();
			for( Integer i = 0; i < numToInsert; i++ ) {
				User u = new User();
				for(String key : nameValue.keySet()){
					u.put(key, nameValue.get(key));
				}
				users.add(u);
			}
			
			if( doInsert ) {
				insert users;
			}
			return users;
	}
	
	public static void createCustomSettings_TriggerStatus(){
		// create Triggerstatus Settings
		TriggerStatus__c triggerStatus = new TriggerStatus__c();
		triggerStatus.AccountGenerateCustomerNumber__c =true;
		triggerStatus.AccountSyncToTimberline__c =true;
		triggerStatus.Forecast_Sync_to_Timberline__c =true;
		triggerStatus.InvoiceAutomaticInvoiceNumber__c =true;
		triggerStatus.InvoiceSyncToTimberline__c =true;
		triggerStatus.InvoiceLineItemAutoCreation__c =true;
		triggerStatus.InvoiceLineItemAutomaticNaming__c =true;
		triggerStatus.InvoiceLineItemAutomaticOrder__c =true;
		triggerStatus.InvoiceTaxGroupSetUp__c =true;
		triggerStatus.JobCopyPMToOwner__c =true;
		triggerStatus.Job_Number_Sequence_Group__c =20140;
		triggerStatus.JobOpportunitySync__c =true;
		triggerStatus.JobSetJobNumber__c =true;
		triggerStatus.JobSyncToTimberline__c =true;
		triggerStatus.LeadPMLeadSync__c =true;
		triggerStatus.OpportunityJobSync__c =true;
		triggerStatus.PMLeadCalculateJobNumber__c =true;
		triggerStatus.PMLeadConversion__c =true;
		triggerStatus.PMLeadLeadSync__c =true;
		triggerStatus.TaskTaskSync__c =true;
		triggerStatus.TaxGroupRefreshFromTimberline__c =true;
		triggerStatus.TaxGroupSetName__c =true;
	insert triggerStatus;
	}

	//create Accounts 
	public static List<Account> createAccounts(Integer numToInsert,
		Boolean doInsert,
		Map<String, Object> nameValue) {
			List<Account> accounts = new List<Account>();
			for( Integer i = 0; i < numToInsert; i++ ) {
				Account a = new Account( Name='Test Account'+String.valueOf(i) );
				a.billingStreet = String.valueOf(i) +'Test Street';
				a.billingcity = 'Test City';
			 	a.billingstate = 'CT';
			 	a.billingpostalcode = '0001';
 				a.phone = '8000000000';
				for(String key : nameValue.keySet()){
					a.put(key, nameValue.get(key));
				}
				accounts.add(a);
			}
			
			if( doInsert ) {
				insert accounts;
			}
			return accounts;
	}
	
	

	//create contacts
	public static List<Contact> createContacts(Integer numToInsert,
	Boolean doInsert,
	Map<String, Object> nameValue,
	Id accountId) {
		List<Contact> contacts = new List<Contact>();
		for( Integer i = 0; i < numToInsert; i++ ) {
			Contact c = new Contact( LastName='Test Contact'+String.valueOf(i)  );
			c.contact_type__c = 'Adjustor';
	        c.firstname = 'semira'+String.valueOf(i);
	        c.Lastname = 'roy'+String.valueOf(i);
	        c.accountId = accountId;
			for(String key : nameValue.keySet()){
				c.put(key, nameValue.get(key));
			}
			contacts.add(c);
		}
		
		if( doInsert ) {
			insert contacts;
		}
		return contacts;
	}
	
		//create Opportunity 
	public static List<Opportunity> createOpportunity (Integer numToInsert,
	Boolean doInsert,
	Map<String, Object> nameValue,
	Id accountId) {
		List<Opportunity> Opportunities  = new List<Opportunity >();
		for( Integer i = 0; i < numToInsert; i++ ) {
			Opportunity  o = new Opportunity(Name='Test Opportunity '+String.valueOf(i)  );
			 o.accountId = accountId;
			 o.Name = 'semira';
			 o.Job_Name__c = 'test';
			 o.Project_Site_Address__c = 'Test Street';
			 o.Project_Site_City__c = 'Los Angeles';
			 o.Project_Site_state__c = 'CA';
			 o.Project_Site_zipcode__c ='90027';
			 o.County__c = 'LA';
			 o.City_of_LA__c = 'Yes';
			 o.StageName ='Qualification';
			 o.CloseDate = System.today().addDays(90);
			for(String key : nameValue.keySet()){
				o.put(key, nameValue.get(key));
			}
			Opportunities.add(o);
		}
		
		if( doInsert ) {
			insert Opportunities;
		}
		return Opportunities;
	}

}