/*
 * @Auhtor: Nikhil (Perficient)
 * @Created: 18.MAR.2015
 * @Desc : Handler class for the Opportunity Split at opportunityTeamMember.
 *       
 * 
 *
 */
      public class Opportunitysplithandler{
      private static Final String AE1= 'Account Executive 1';
      private static Final String AE2= 'Account Executive 2';
      private static final Id ovId=[select id from OpportunitySplitType where DeveloperName =: 'Overlay'].id;  
      public static void afterEventMethod(Map<Id, OpportunityTeamMember> oldtriggerotm, Map<Id, OpportunityTeamMember> newtriggerotm, boolean Isupdate) {
          if(Isupdate){
          
          }else{
          insertoppsplit(newtriggerotm.values());
          }
      }
      
      public static void insertoppsplit(List<OpportunityTeamMember> optm){
      List<OpportunitySplit> oppts = new List<OpportunitySplit>();  
        
      for(OpportunityTeamMember o : optm){
        
       OpportunitySplit os1 = new OpportunitySplit();  
       os1.OpportunityId = o.OpportunityId ;  
       os1.SplitOwnerId = o.UserId;
       os1.SplitTypeId=ovId;
       if(o.TeamMemberRole.equalsIgnoreCase(AE1)){  
       os1.SplitPercentage = 100;  
       oppts.Add(os1);
       }else if(o.TeamMemberRole.equalsIgnoreCase(AE2)){
       os1.SplitPercentage = 50;  
       oppts.Add(os1);
       }  
      }
      
      if(!oppts.IsEmpty())insert oppts; //Do exception Handling here 
      
      }  
       
      
      

}