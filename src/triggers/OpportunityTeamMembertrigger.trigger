/*
 * @Auhtor: Nikhil (Perficient)
 * @Created: 18.MAR.2015
 * @Desc : Trigger for the Opportunity Split at opportunityTeamMember.
 *       
 * 
 *
 */
trigger OpportunityTeamMembertrigger on OpportunityTeamMember(after insert,after update) {
    if (Trigger.IsAfter) {
        if (Trigger.isInsert) {
            //Opportunitysplithandler.afterEventMethod(Trigger.oldMap, Trigger.newMap,false);
        }
        if (Trigger.isUpdate) {
            //Opportunitysplithandler.afterEventMethod(Trigger.oldMap, Trigger.newMap,true);
        }
        if (Trigger.isDelete) {}
    }
}