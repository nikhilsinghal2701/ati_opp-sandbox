trigger EmailUpdateOnJob on Job__c (after update) {

  //Limit the size of list by using Sets which do not contain duplicate elements
  set<Id> ContactIds = new set<Id>();
  set<ID>ProjectContactID = new set<Id>();
  
  Set<ID> updateContactIds = new Set<ID>();

  String Email = '';
  ID jobId = null;

  
  //When adding new Jobss or updating existing Jobs
  if(trigger.isUpdate){
  	for(Job__c p : trigger.new){
      email = p.Add_Correct_Email__c;
      jobId = p.id;
      
      if(Trigger.oldmap.get(p.id).Add_Correct_Email__c != email){
      	ContactIds.add(p.Project_site_contact_name__c);
      }     
            
      if(Trigger.oldmap.get(p.id).Project_site_contact_name__c != p.Project_site_contact_name__c){      	
      	updateContactIds.add(p.Id);
      }
      else if(Trigger.oldmap.get(p.id).Project_Site_Contact_Account__c != p.Project_Site_Contact_Account__c){
      	updateContactIds.add(p.Id);
      }
      else if(Trigger.oldmap.get(p.id).Account__c != p.Account__c){
      	updateContactIds.add(p.Id);
      }
      else if(Trigger.oldmap.get(p.id).Contact__c != p.Contact__c){
      	updateContactIds.add(p.Id);
      }
    }

if (updateContactIds.size() > 0)
{
	List<Job__c> jobs = [SELECT ID, Contact__c, Account__c, Project_site_contact_name__c, Project_Site_Contact_Account__c, (SELECT ID, Contact__c FROM Job_Contacts__r), (SELECT ID, Account__c FROM Job_Accounts__r) FROM Job__c WHERE ID in :updateContactIds];
	List<Job_Contact__c> addJobContacts = new List<Job_Contact__c>();
	List<Job_Account__c> addJobAccounts = new List<Job_Account__c>();
	for (Job__c job : jobs)
	{		
		// Check and change
		// check Contact existed
		boolean SameAsPrimaryContact = false;
		boolean SameAsPrimaryAccount = false;
		boolean contactExist = false;
		boolean projectContactExist = false;
		boolean accountExist = false;
		boolean projectAccountExist = false;
		
		if(job.Contact__c == job.Project_site_contact_name__c)
			SameAsPrimaryContact = true;
			system.debug('This is SameAsPrimaryContact:::::::::::::::'+ SameAsPrimaryContact);
		if(job.Account__c == job.Project_Site_Contact_Account__c)
			SameAsPrimaryAccount = true;
			system.debug('This is SameAsPrimaryAccount:::::::::::::::'+ SameAsPrimaryAccount);
			
		for(job_contact__c con : [select id, contact__c from job_contact__c where id in: job.job_contacts__r])
		{
			if (con.contact__c == job.Contact__c && con.contact__c != null)
			{
				contactExist = true;
				system.debug('This is ContactExist:::::::::::::::'+ contactExist);
			}if (con.contact__c == job.Project_site_contact_name__c && con.contact__c != null && job.Contact__c != job.Project_site_contact_name__c)
			{
				projectContactExist = true;
				system.debug('This is projectContactExist:::::::::::::::'+ projectContactExist);
			}
			if (contactExist && projectContactExist)
			{
				break;
			}
		}
		for(job_account__c acct : [select id, account__c from job_account__c where id in: job.job_accounts__r])
		{
			if (acct.account__c == job.Account__c && acct.account__c != null)
			{
				accountExist = true;
				system.debug('This is accountExist:::::::::::::::'+ accountExist);
			}if (acct.account__c == job.Project_Site_Contact_Account__c && acct.account__c != null && job.Account__c != job.Project_Site_Contact_Account__c)
			{
				projectAccountExist = true;
				system.debug('This is projectAccountExist:::::::::::::::'+ projectAccountExist);
			}
			if (accountExist && projectAccountExist)
			{
				break;
			}
		}
		
		if (!contactExist){
			Job_contact__c jobContact = new Job_Contact__c();
			jobContact.Job__c = job.Id;
			jobContact.Contact__c = job.Contact__c;
			addJobContacts.add(jobContact);
		}
		
		if (!projectContactExist && !SameAsPrimaryContact)
		{
			Job_contact__c jobContact = new Job_Contact__c();
			jobContact.Job__c = job.Id;
			jobContact.Contact__c = job.Project_site_contact_name__c;
			addJobContacts.add(jobContact);			
		}
		if (!accountExist){
			Job_Account__c jobAccount = new Job_Account__c();
			jobAccount.Job__c = job.Id;
			jobAccount.account__c = job.account__c;
			addJobAccounts.add(jobAccount);
		}
		
		if (!projectAccountExist && !SameAsPrimaryAccount)
		{
			Job_Account__c jobAccount = new Job_Account__c();
			jobAccount.Job__c = job.Id;
			jobAccount.Account__c = job.Project_Site_Contact_Account__c;
			addJobAccounts.add(jobAccount);			
		}
		
		// Check Project Contact is exsted.
	}
	if (addJobContacts.size() > 0 )
	{
		insert addJobContacts;
	}
	if(addJobAccounts.size() > 0)
	{
		insert addJobAccounts;
	}
}

 List<Contact> contactNeedActions = new list<contact>();

    if (ContactIds.size() > 0){
    	
    	for(Contact a : [Select Id, email from Contact where Id IN :ContactIds]){

	      System.debug('Email is::::::::::::::::::::::::::::::::::::::' + a.email);
	      System.debug('Contact Id is::::::::::::::::::::::::::::::::::::::' + a.Id);
	      
	      a.email = email;
	      
	       contactNeedActions.add(a);

    	}
    	
    	update contactNeedActions;
    }
   
  }
}