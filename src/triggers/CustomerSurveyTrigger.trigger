trigger CustomerSurveyTrigger on Customer_Survey__c (after insert) {
    
    //Limit the size of list by using Sets which do not contain duplicate elements
  set<Id> SurveyIds = new set<Id>();


  List<Id> jobID = new List<id>();

  
  //When adding new survey or updating existing survey
  if(trigger.isInsert || trigger.isUpdate){
    for(Customer_Survey__c p : trigger.new){
      jobID.add(p.job__c);      
      SurveyIds.add(p.id);
    }
   

 List<Customer_Survey__c> SurveyUpdate = new list<Customer_Survey__c>();

    if (SurveyIds.size() > 0){
    
    For(Job__c FetchJobInfo : [select id, Project_Manager__c, Project_Manager__r.ManagerId, Superintendent_Ops__c, Operations_Manager__c from Job__c where id IN:jobID]){
        
        for(Customer_Survey__c a : [Select Id, Project_Manager__c, Operations_Manager__c, Superintendent__c from Customer_Survey__c where Id IN :SurveyIds]){

          
          a.Project_Manager__c = FetchJobInfo.Project_Manager__c;
          a.Operations_Manager__c = FetchJobInfo.Operations_Manager__c;
          a.Superintendent__c = FetchJobInfo.Superintendent_Ops__c;
          a.manager__c = FetchJobInfo.Project_Manager__r.ManagerId;
          
           SurveyUpdate.add(a);

        }
    }
        update SurveyUpdate;
    }
   
  }
}