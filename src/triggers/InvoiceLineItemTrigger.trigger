trigger InvoiceLineItemTrigger on Invoice_Line_Item__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
    TriggerStatus__c cfg = TriggerConfig.raw;
    
    /////////////////////////////////////////////////////
    // Order change
    // This only runs when one item is changed.
    if(cfg.InvoiceLineItemAutomaticOrder__c && Trigger.isBefore && Trigger.isUpdate && Trigger.new.size() < 2 && !InvoiceLineItemControl.IsRuning)
    {
        System.debug(Logginglevel.WARN, 'InvoiceLineItemTrigger: Running automatic Order');
        
        InvoiceLineItemControl.setOrder(Trigger.new[0], Trigger.old[0]); 
    }
    
    //////////////////////////////////////////////////////
    // Name Convination
    if (cfg.InvoiceLineItemAutomaticNaming__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
    {
        System.debug(Logginglevel.WARN, 'InvoiceLineItemTrigger: Running automatic Name generation');
        
        InvoiceLineItemControl.setInvoiceName(Trigger.new);
    }
}