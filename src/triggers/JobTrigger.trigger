trigger JobTrigger on Job__c (after delete, after insert, after undelete, 
        after update, before delete, before insert, before update) {
   
  
    TriggerStatus__c cfg = TriggerConfig.raw;
    
    
    ///////////////////////////////////////////////////////
    // PREVENT DUPLICATE JOB NUMBERS ON CLONE
    if (cfg.JobSetJobNumber__c && Trigger.isBefore && Trigger.isInsert) {
        
        System.debug(Logginglevel.INFO, 'JobTrigger: Running JobControl.preventDuplicateJobNumbers...');
        JobControl.preventDuplicateJobNumbers(Trigger.new);
        
    }
    
    ///////////////////////////////////////////////////////
    // Update Job Number
    if (cfg.JobSetJobNumber__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        
        System.debug(Logginglevel.INFO, 'JobTrigger: Running JobControl.setJobNumberSequential...');
        JobControl.setJobNumberSequential(Trigger.new, (Trigger.isInsert) ? null : Trigger.oldMap);
        
        System.debug(Logginglevel.INFO, 'JobTrigger: Running JobControl.recalculateJobNumbers...');
        JobControl.recalculateJobNumbers(Trigger.new, (Trigger.isInsert) ? null : Trigger.oldMap);
        
        System.debug(Logginglevel.INFO, 'JobTrigger: Running JobControl.setGLAccountPrefix...');
        JobControl.setGLAccountPrefix(Trigger.new);
        
    }
    
 /**************************************************************************************************
    
    ///////////////////////////////////////////////////////
    // REMOVE SYNC'D RECORDS ON INSERT
    if (RecordSync_JobToOppty.getClearSyncs() && cfg.JobOpportunitySync__c && Trigger.isBefore && Trigger.isInsert) {
        System.debug(Logginglevel.INFO, 'JobTrigger: Clearing references to synchronized records');
        for (Job__c j : Trigger.new) {
            j.Synced_Opportunity__c = null;
        }
    }
    
    
    ///////////////////////////////////////////////////////
    // Synchronize Job data with an Opportunity
    if (cfg.JobOpportunitySync__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        RecordSync_JobToOppty.sync(Trigger.new);
    }
 
 **********************************************************************************************************/   
    
    //////////////////////////////////////////////////////
    // TIMBERLINE JOB SYNC
    if (cfg.JobSyncToTimberline__c && Trigger.isBefore && Trigger.isUpdate && TimberlineSynch.jobSynching == false)
    {   
        // List of Ids of Jobs to be send to Timberline 
        Set<Id> sendToTimberline = new Set<Id>();
        
        system.debug('TRying to Synch Jobs');
            
        for(Job__c newJob : Trigger.new)
        {
            // Make sure the flag is set to true
            if((newJob.Stage__c == 'Won' && newJob.Status__c == 'Active') || newJob.Sync_to_Timberline__c )
            {
                // Set the flag to false
                newJob.Sync_to_Timberline__c = false;
                
                // add the Job to the set
                sendToTimberline.add(newJob.id);
            }
        }
            
        // Send all the elements
        if(sendToTimberline.size() > 0)
        {
            // Synch the jobs
            TimberlineSynch.synchJobs(sendToTimberline);
        }

    }
    
    //////////////////////////////////////////////////////
    // TIMBERLINE Forecast SYNC
    if (cfg.Forecast_Sync_to_Timberline__c && Trigger.isBefore && Trigger.isUpdate && TimberlineSynch.jobSynching == false)
    {   
        // List of Ids of Jobs to be send to Timberline 
        Set<Id> sendToTimberline = new Set<Id>();
        
        system.debug('Trying to Synch Forecast');
            
        for(Job__c newJob : Trigger.new)
        {
            // Make sure the flag is set to true
            if(newJob.Update_Forecast__c)
            {
                // Set the flag to false
                newJob.Update_Forecast__c = false;
                
                // add the Job to the set
                sendToTimberline.add(newJob.id);
            }
        }
            
        // Send all the elements
        if(sendToTimberline.size() > 0)
        {
            // Synch the jobs
            TimberlineSynch.synchForecast(sendToTimberline);
        }

    }
    
    
    //////////////////////////////////////////////////////
    // TIMBERLINE UPDATE FORECASTS GLOBALLY
    if (cfg.Forecast_Sync_to_Timberline__c && Trigger.isBefore && Trigger.isUpdate && TimberlineSynch.jobSynching == false)
    {   
        JobControl.globalRefreshForecasts(Trigger.new);
    }
    
}