trigger Cirrus_Files_Job on Job__c(before insert, after insert, after undelete, after update) {
    IGD.SyncBatcher.syncFiles();
}