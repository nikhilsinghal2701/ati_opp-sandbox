/**
*    This trigger copies the Jobs Project Manager into the Owner field

    @author American Data Company
    @date 7/27/2012
*/
trigger ManagerToOwnerCopy on Job__c (before insert, before update) {
    
    if (! TriggerConfig.raw.JobCopyPMToOwner__c) {
        return;
    }
    
    //get all of the jobs associated with the trigger
    Job__c[] jobs = Trigger.new;
    
    //loop through all of the jobs
    for (Job__c j : jobs)
    {
        //if project manager isn't empty
        if(j.Project_Manager__c != null)
        {
            //copy the manager ID to the owner
            j.OwnerId = j.Project_Manager__c;
        }
    }
}