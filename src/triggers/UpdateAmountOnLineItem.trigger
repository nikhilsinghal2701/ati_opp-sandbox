trigger UpdateAmountOnLineItem on Expense_Line_Item__c (after update, before delete, after delete) {

set<id>expenseIds = new set<id>();
set<id>lineItemIds = new set<id>();
set<string>names = new set<String>();

if(trigger.Isupdate)
{

    for(Expense_Line_Item__c opp: trigger.new)
    {
       if(Trigger.oldMap.get(opp.id).Amount__c != Trigger.newMap.get(opp.id).Amount__c  )
      { 
       expenseIds.add(opp.expense__c);
        lineItemIds.add(opp.id);
        names.add(opp.name);
      }
    }
}
if(trigger.isDelete)
{

    for(Expense_Line_Item__c p : trigger.old)
    {
    
        expenseIds.add(p.expense__c);
        lineItemIds.add(p.id);
        names.add(p.name);
    
    system.debug('IIIIIIIIIIIIIIIIIIII'+trigger.old);
    System.debug('LLLLLLLLLLLLLLLLLLLLL'+lineItemIds);
    System.debug('LLLLLLLLLLLLLLLLLLLLL'+names);
           
    }
}

map<id,double> Accountmap = new map<id,double>();

Integer count = 0;
//Decimal total1 = 0.0;
for(AggregateResult q : [select sum(amount__c)total1, count(id)count1, expense__c from Expense_Line_Item__c where expense__c IN: expenseIds and name IN: names group by expense__c])
{
    system.debug('Total::::::::::::::::::::::::::::::::::::::'+Accountmap.put((id)q.get('expense__c'),(double)q.get('total1')));
    count++;
    system.debug('count:::::::::::::::::::::::::::::::::::' + count);

}

List<Expense_Line_Item__c> LineItemsToUpdate = new List<Expense_Line_Item__c>();
if( LineItemRunner.firstRun)
{
    Double amount1 = 0.0;
    integer counter = 0;
    for(Expense_Line_Item__c i : [Select Id,amount__c,expense__c from Expense_Line_Item__c where expense__c IN :expenseIds and name IN: names]){
        amount1 = accountMap.get(i.expense__c);
        counter++;
        
            }
    system.debug('amount:::::::::::::::::::::::::::::::::::' + amount1);
    system.debug('counter:::::::::::::::::::::::::::::::::::' + counter);
    Double total = 0.0;
    
    if(trigger.isUpdate && count>0)
       total = amount1/counter;  
       
    if(trigger.isDelete && count>0){
        counter = counter - 1;
        if(counter>0)
        	total = amount1/counter;
       	else
       		total = 0.0;
    }
       
       system.debug('2nd total:::::::::::::::::::::::::::::::::::' + total);
     
    for(Expense_Line_Item__c o : [Select Id,amount__c,expense__c from Expense_Line_Item__c where expense__c IN :expenseIds and name IN: names])
    {
        o.amount__c=total;
        LineItemsToUpdate.add(o);
    }
     LineItemRunner.firstrun=false;
}
update LineItemsToUpdate;
}