trigger Tax_Group on Tax_Group__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    
    TriggerStatus__c cfg = TriggerConfig.raw;
    
    if (! TriggerConfig.raw.TaxGroupSetName__c) {
        return;
    }
    
    if (cfg.TaxGroupSetName__c && trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        for (Tax_Group__c t : Trigger.new) {
            t.Name = t.Timberline_Id__c;
        }
    }
    
    if (cfg.TaxGroupRefreshFromTimberline__c && trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        system.debug('Running Tax_Group Trigger');
        TaxGroupControl.taxGroupRefreshTrigger(Trigger.new);
    }
    
    system.debug('NOT Running Tax_Group Trigger'); 
    
}