trigger TaskTrigger on Task (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    
    // Synchronize Task data with other Tasks
    if (TriggerConfig.raw.TaskTaskSync__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        RecordSync_Task.sync(Trigger.new);
    }
    
}